-- phpMyAdmin SQL Dump
-- version 4.5.1
-- http://www.phpmyadmin.net
--
-- Servidor: 127.0.0.1
-- Tiempo de generación: 10-07-2017 a las 15:20:48
-- Versión del servidor: 10.1.19-MariaDB
-- Versión de PHP: 5.5.38

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Base de datos: `agenda`
--

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `contactos`
--

CREATE TABLE `contactos` (
  `id` int(11) NOT NULL,
  `nombre` varchar(200) COLLATE utf8_spanish_ci NOT NULL,
  `apellidos` varchar(200) COLLATE utf8_spanish_ci NOT NULL,
  `email` varchar(200) COLLATE utf8_spanish_ci NOT NULL,
  `telefono` varchar(200) COLLATE utf8_spanish_ci NOT NULL,
  `imagen` varchar(200) COLLATE utf8_spanish_ci NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_spanish_ci;

--
-- Volcado de datos para la tabla `contactos`
--

INSERT INTO `contactos` (`id`, `nombre`, `apellidos`, `email`, `telefono`, `imagen`) VALUES
(1, 'Naomi', 'Vladimir Watts', 'mi.fringilla@augueporttitorinterdum.org', '010-423-7423', 'imagen1.jpg'),
(3, 'Patricia', 'Karyn Jacobson', 'magna@lacusQuisque.net', '080-672-3866', 'imagen1.jpg'),
(4, 'Axel', 'Porter Pennington', 'natoque.penatibus.et@nonenimMauris.com', '046-360-3986', 'imagen1.jpg'),
(5, 'Clark', 'Ann Perry', 'leo.Morbi@velsapien.co.uk', '079-803-0125', 'imagen1.jpg'),
(6, 'Bree', 'Aquila Griffith', 'amet.ultricies.sem@ante.org', '061-669-3177', 'imagen1.jpg'),
(7, 'Erasmus', 'Yuri Johns', 'tempus.lorem@utcursusluctus.co.uk', '016-400-9687', 'imagen1.jpg'),
(8, 'Imelda', 'Angela Graves', 'malesuada@ante.org', '013-768-8967', 'imagen1.jpg'),
(9, 'Zorita', 'Keiko Melton', 'at.sem.molestie@ullamcorpervelitin.ca', '084-737-7818', 'imagen1.jpg'),
(10, 'Raphael', 'Kendall Wheeler', 'ut@etcommodoat.co.uk', '060-703-0275', 'imagen1.jpg'),
(11, 'Mason', 'Violet Moody', 'lobortis@dolorsit.org', '001-177-8306', 'imagen1.jpg'),
(12, 'Aiko', 'Azalia Boyer', 'pede@blandit.org', '027-304-4647', 'imagen1.jpg'),
(13, 'Kaden', 'Hakeem Carrillo', 'parturient.montes@Nullamlobortisquam.edu', '002-901-1047', 'imagen1.jpg'),
(14, 'Mary', 'Ila Monroe', 'turpis@elementumsemvitae.net', '079-640-3056', 'imagen1.jpg'),
(15, 'Joshua', 'Casey Rollins', 'molestie.tortor.nibh@lectus.com', '020-216-6189', 'imagen1.jpg'),
(16, 'Aurora', 'Hilary Ellison', 'Integer@enimSed.co.uk', '025-030-1650', 'imagen1.jpg'),
(17, 'Neil', 'Derek Rogers', 'Maecenas.malesuada@sit.net', '068-046-4531', 'imagen1.jpg'),
(18, 'Nolan', 'Zahir Foster', 'lorem.eget.mollis@pedeCras.org', '051-697-8920', 'imagen1.jpg'),
(19, 'Travis', 'Jaden Olsen', 'ligula.consectetuer.rhoncus@at.edu', '086-022-8933', 'imagen1.jpg'),
(20, 'Delilah', 'Liberty Small', 'a@ligula.ca', '013-421-7973', 'imagen1.jpg'),
(21, 'Burton', 'Castor Rhodes', 'dictum@lectus.edu', '050-761-3455', 'imagen1.jpg'),
(22, 'Selma', 'Kibo Wiggins', 'at.velit@Quisque.net', '045-762-2069', 'imagen1.jpg'),
(23, 'Kieran', 'Marsden Blackburn', 'consequat@velitPellentesque.edu', '017-399-6412', 'imagen1.jpg'),
(24, 'Hiram', 'Kane Greer', 'Etiam.bibendum.fermentum@elitNullafacilisi.ca', '084-522-9024', 'imagen1.jpg'),
(25, 'Yoshi', 'Sybill Ashley', 'pharetra.Nam.ac@amagnaLorem.org', '069-781-7795', 'imagen1.jpg'),
(26, 'Anastasia', 'Whitney Rivers', 'sem@adipiscinglacusUt.com', '049-837-3251', 'imagen1.jpg'),
(27, 'Kenyon', 'Abdul Rivera', 'ac@orciinconsequat.edu', '012-913-3131', 'imagen1.jpg'),
(28, 'Cedric', 'Meghan Cobb', 'Proin.vel@luctusCurabituregestas.org', '056-488-6318', 'imagen1.jpg'),
(29, 'Bianca', 'Ayanna Tyler', 'Duis.cursus@Maurisutquam.co.uk', '094-987-8099', 'imagen1.jpg'),
(30, 'Sasha', 'Brody Oneil', 'non.quam@Praesent.edu', '063-317-9967', 'imagen1.jpg'),
(31, 'Jakeem', 'Jena Cummings', 'mauris.ipsum.porta@nonenim.co.uk', '025-039-4732', 'imagen1.jpg'),
(32, 'Eaton', 'Rae Mckenzie', 'felis.purus.ac@sapien.co.uk', '088-231-3656', 'imagen1.jpg'),
(33, 'Nina', 'Danielle Nieves', 'vel@enimcondimentum.edu', '076-297-4287', 'imagen1.jpg'),
(34, 'Talon', 'Hilary Hodges', 'nibh@Integer.edu', '037-678-8799', 'imagen1.jpg'),
(35, 'Merritt', 'Adrienne Bartlett', 'lorem@Proinvel.org', '023-362-5681', 'imagen1.jpg'),
(36, 'Mason', 'Debra Barlow', 'non@IntegermollisInteger.org', '022-749-2621', 'imagen1.jpg'),
(37, 'Sage', 'Jesse Estrada', 'lacus.varius.et@sed.edu', '057-316-6890', 'imagen1.jpg'),
(38, 'Cora', 'Ethan Bond', 'sed@estarcu.net', '077-133-1656', 'imagen1.jpg'),
(39, 'Benjamin', 'Natalie Sloan', 'nulla.Donec.non@enim.com', '008-502-3295', 'imagen1.jpg'),
(40, 'Angela', 'Linus Mclaughlin', 'luctus.et.ultrices@ettristique.co.uk', '003-308-1424', 'imagen1.jpg'),
(41, 'Kirk', 'Germaine Glass', 'egestas.Sed@elitsed.com', '015-101-5700', 'imagen1.jpg'),
(42, 'Alfreda', 'Lois Cervantes', 'nulla@magna.co.uk', '065-070-8308', 'imagen1.jpg'),
(43, 'Bert', 'Geoffrey Washington', 'egestas@nequeseddictum.net', '020-058-5416', 'imagen1.jpg'),
(44, 'Eden', 'Carissa Cummings', 'Cras@risusvarius.ca', '050-645-2768', 'imagen1.jpg'),
(45, 'Britanni', 'Oliver Hobbs', 'et@risusNulla.net', '041-067-9006', 'imagen1.jpg'),
(46, 'Yvonne', 'Ora Dillard', 'adipiscing.ligula.Aenean@tacitisociosqu.ca', '006-380-0840', 'imagen1.jpg'),
(47, 'Geoffrey', 'Pandora Buck', 'sit.amet.luctus@fringillapurus.net', '067-937-1698', 'imagen1.jpg'),
(48, 'Myra', 'Travis Benjamin', 'Proin.ultrices@nequesed.com', '000-947-0417', 'imagen1.jpg'),
(49, 'Lyle', 'Griffith Baxter', 'quis@necanteblandit.net', '020-360-1065', 'imagen1.jpg'),
(50, 'Laurel', 'Allegra Blair', 'rutrum.urna@nonegestas.edu', '084-669-7229', 'imagen1.jpg'),
(51, 'Chiquita', 'Quincy Holland', 'In.faucibus@a.org', '010-752-2904', 'imagen1.jpg'),
(52, 'Melyssa', 'Baker Glover', 'auctor.odio@convallisin.ca', '082-716-9562', 'imagen1.jpg'),
(53, 'Armand', 'George Cross', 'pellentesque.a@purusaccumsan.net', '048-566-1219', 'imagen1.jpg'),
(54, 'Gloria', 'Nomlanga Kemp', 'ultricies.adipiscing@sem.ca', '070-505-0092', 'imagen1.jpg'),
(55, 'Cherokee', 'Brynne Barry', 'consectetuer.cursus.et@primisin.co.uk', '036-089-9430', 'imagen1.jpg'),
(56, 'Jennifer', 'Wayne Rodriquez', 'sem@ametrisusDonec.co.uk', '004-848-8121', 'imagen1.jpg'),
(57, 'Noble', 'Bree Pacheco', 'felis@Proinsedturpis.com', '072-321-8443', 'imagen1.jpg'),
(58, 'Jessamine', 'Arthur Mccall', 'Sed.malesuada.augue@bibendumullamcorperDuis.ca', '042-055-0348', 'imagen1.jpg'),
(59, 'Jocelyn', 'Hilel Strickland', 'nec.tempus@adipiscingfringillaporttitor.org', '080-921-3481', 'imagen1.jpg'),
(60, 'Patience', 'Evelyn Mooney', 'Cum@aenimSuspendisse.ca', '046-520-5409', 'imagen1.jpg'),
(61, 'Winifred', 'Marshall Anderson', 'tristique.senectus.et@tinciduntadipiscing.org', '007-075-2078', 'imagen1.jpg'),
(62, 'Natalie', 'Quemby Golden', 'lacinia@Donecatarcu.org', '058-820-0540', 'imagen1.jpg'),
(63, 'Helen', 'Grant Maddox', 'Proin@diamSeddiam.com', '052-521-2496', 'imagen1.jpg'),
(64, 'Tate', 'Hedley Pearson', 'tellus.imperdiet@justosit.org', '079-396-3610', 'imagen1.jpg'),
(65, 'Mari', 'Kerry Stevens', 'congue@orciluctuset.net', '079-233-1748', 'imagen1.jpg'),
(66, 'Hedda', 'Nicole Whitley', 'nunc.ac@dis.co.uk', '092-723-7859', 'imagen1.jpg'),
(67, 'Eugenia', 'Morgan Hansen', 'lorem@scelerisqueneque.com', '052-809-2002', 'imagen1.jpg'),
(68, 'Elton', 'Brielle Christensen', 'morbi@Nam.com', '037-266-4800', 'imagen1.jpg'),
(69, 'Flavia', 'Kaitlin Ruiz', 'ac.ipsum@PhasellusornareFusce.edu', '095-707-1862', 'imagen1.jpg'),
(70, 'Raven', 'Sopoline Mosley', 'turpis@vulputateposuerevulputate.co.uk', '088-705-6297', 'imagen1.jpg'),
(71, 'Xyla', 'Grant Slater', 'velit.egestas.lacinia@fermentum.org', '038-761-7986', 'imagen1.jpg'),
(72, 'Elton', 'Aline Kramer', 'in.dolor.Fusce@acmetus.com', '016-235-2716', 'imagen1.jpg'),
(73, 'Risa', 'Travis Gray', 'Nulla.eu@metuseu.edu', '051-894-2778', 'imagen1.jpg'),
(74, 'Hop', 'Dana Fleming', 'diam.luctus@erat.net', '096-496-8090', 'imagen1.jpg'),
(75, 'Whoopi', 'Shad Greene', 'ultricies.ornare@justofaucibus.ca', '038-035-8548', 'imagen1.jpg'),
(76, 'Allistair', 'Lenore Gross', 'magna@sed.com', '051-760-8157', 'imagen1.jpg'),
(77, 'Rana', 'Christine Short', 'consectetuer@Aeneansedpede.net', '048-850-2236', 'imagen1.jpg'),
(78, 'Sawyer', 'Keely Merritt', 'interdum@diamSed.net', '057-816-1096', 'imagen1.jpg'),
(79, 'Idona', 'Libby Malone', 'ultrices@Donecconsectetuermauris.org', '078-028-0862', 'imagen1.jpg'),
(80, 'Bell', 'Thaddeus Bradford', 'adipiscing@eteuismod.co.uk', '031-910-3680', 'imagen1.jpg'),
(81, 'Wesley', 'Richard Ashley', 'sed.turpis.nec@CuraeDonectincidunt.ca', '052-564-6161', 'imagen1.jpg'),
(82, 'Sybill', 'Winifred Molina', 'magna.Lorem.ipsum@fringilla.org', '084-473-4866', 'imagen1.jpg'),
(83, 'Noel', 'Jacqueline Sparks', 'hendrerit.id@temporlorem.co.uk', '090-555-3190', 'imagen1.jpg'),
(84, 'Wayne', 'Xaviera Spence', 'accumsan@loremDonecelementum.ca', '066-985-2004', 'imagen1.jpg'),
(85, 'Lev', 'Arsenio Hall', 'mattis.ornare@auctorquis.net', '081-893-4091', 'imagen1.jpg'),
(86, 'Silas', 'Dante Oneill', 'Fusce.feugiat@habitantmorbi.edu', '052-979-4518', 'imagen1.jpg'),
(87, 'Jane', 'Yen Aguilar', 'Fusce@acmi.ca', '070-162-5329', 'imagen1.jpg'),
(88, 'Kalia', 'Avye Simmons', 'Proin.vel.arcu@mollislectus.edu', '084-847-1948', 'imagen1.jpg'),
(89, 'Stuart', 'Allistair Swanson', 'non.bibendum@mollis.com', '069-838-4333', 'imagen1.jpg'),
(90, 'McKenzie', 'Amaya Dillon', 'fringilla.porttitor.vulputate@lobortisquama.edu', '044-823-5763', 'imagen1.jpg'),
(91, 'Meredith', 'Alexa Blankenship', 'sodales@ornare.edu', '057-601-9025', 'imagen1.jpg'),
(92, 'Kylee', 'Chadwick Cote', 'sem.vitae@erat.org', '036-267-5847', 'imagen1.jpg'),
(93, 'Victoria', 'Dorothy Kim', 'massa.Vestibulum.accumsan@imperdietullamcorper.edu', '009-026-0369', 'imagen1.jpg'),
(94, 'Celeste', 'Lila Fitzpatrick', 'Donec.nibh@felis.ca', '006-520-6657', 'imagen1.jpg'),
(95, 'Maya', 'Shea Zamora', 'Integer@purus.ca', '015-251-8922', 'imagen1.jpg'),
(96, 'Haley', 'Chaim Mcguire', 'ligula.consectetuer@pellentesqueSeddictum.com', '008-474-6497', 'imagen1.jpg'),
(97, 'Piper', 'Shea Singleton', 'iaculis@ultricessitamet.ca', '005-645-1490', 'imagen1.jpg'),
(98, 'Avye', 'April Barber', 'ac.eleifend.vitae@cursusluctus.edu', '069-250-7872', 'imagen1.jpg'),
(99, 'Ignatius', 'Armand Fry', 'ligula@temporestac.com', '037-318-4651', 'imagen1.jpg'),
(100, 'Aurelia', 'Stephen Mueller', 'consectetuer.adipiscing.elit@lobortis.co.uk', '071-291-4123', 'imagen1.jpg'),
(101, 'hola', '', '', '', ''),
(102, 'David', 'Fraj Blesa', 'davidfraj@gmail.com', '555123456', ''),
(103, 'ciccio', 'apellidos', 'email', 'telefono', ''),
(104, 'ciccio', 'apellidos', 'email', 'telefono', ''),
(105, 'David', 'Fraj Blesa', 'davidfraj@gmail.com', '555123456', '');

--
-- Índices para tablas volcadas
--

--
-- Indices de la tabla `contactos`
--
ALTER TABLE `contactos`
  ADD PRIMARY KEY (`id`);

--
-- AUTO_INCREMENT de las tablas volcadas
--

--
-- AUTO_INCREMENT de la tabla `contactos`
--
ALTER TABLE `contactos`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=106;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
