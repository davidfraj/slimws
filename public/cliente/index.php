<?php  
//Fichero: c:\xampp\htdocs\slim\public\cliente\index.php

// llamo a diferentes funciones con PHP
require('funciones.php');

//Establezco la URL DE MI WEB service
$url="http://192.168.1.58/slim/public/contactos";

//Vamos a ver que hacer, si elijo borrar
$accion='listado';
if(isset($_GET['accion'])){
	$accion=$_GET['accion'];
	if($_GET['accion']=='borrar'){
		$id=$_GET['id'];
		echo curl_del($url.'/'.$id);
	}
	if($_GET['accion']=='insertar'){
		$json=http_build_query($_POST);
		echo curl_ins($url, $json);
	}
	if($_GET['accion']=='ver'){
		$id=$_GET['id'];
		$url.='/'.$id;
	}
	if($_GET['accion']=='modificar'){
		$json=http_build_query($_POST);
		$id=$_POST['id'];
		$url.='/'.$id;
		curl_mod($url, $json);
	}
}

//Esto es para mostrar los datos, usando verbos GET
$datosCrudo=file_get_contents($url);
$datos=json_decode($datosCrudo);

?>

<!DOCTYPE html>
<html lang="en">
<head>
	<meta charset="UTF-8">
	<title>Indice del cliente de mi API REST para probar cosas</title>
</head>
<body>
	<hr>

	<?php if($accion=='ver'){ ?>

	<form method="post" action="index.php?accion=modificar">
		<input type="text" name="nombre" placeholder="nombre" value="<?php echo $datos[0]->nombre ?>">
		<input type="text" name="apellidos" placeholder="apellidos" value="<?php echo $datos[0]->apellidos ?>">
		<input type="text" name="email" placeholder="email" value="<?php echo $datos[0]->email ?>">
		<input type="text" name="telefono" placeholder="telefono" value="<?php echo $datos[0]->telefono ?>">
		<input type="hidden" name="id" value="<?php echo $datos[0]->id ?>">
		<input type="submit" value="guardar">
	</form>

	<?php }else{ ?>

	<form method="post" action="index.php?accion=insertar">
		<input type="text" name="nombre" placeholder="nombre">
		<input type="text" name="apellidos" placeholder="apellidos">
		<input type="text" name="email" placeholder="email">
		<input type="text" name="telefono" placeholder="telefono">
		<input type="submit" value="insertar">
	</form>

	<?php } ?>

	<hr>
	<table>
		<tr>
			<th>Nombre</th>
			<th>Apellidos</th>
			<th>Email</th>
			<th>Telefono</th>
			<th>Acciones</th>
		</tr>
		<?php foreach ($datos as $contacto) { ?>
			<tr>
				<td><?php echo $contacto->nombre; ?></td>
				<td><?php echo $contacto->apellidos; ?></td>
				<td><?php echo $contacto->email; ?></td>
				<td><?php echo $contacto->telefono; ?></td>
				<th>
					<a href="index.php?accion=ver&id=<?php echo $contacto->id;?>">Ver / Modificar</a>
					-
					<a href="index.php?accion=borrar&id=<?php echo $contacto->id;?>">Borrar</a>
				</th>
			</tr>
		<?php } ?>
	</table>
	
</body>
</html>