<?php
if (PHP_SAPI == 'cli-server') {
    // To help the built-in PHP dev server, check if the request was actually for
    // something which should probably be served as a static file
    $url  = parse_url($_SERVER['REQUEST_URI']);
    $file = __DIR__ . $url['path'];
    if (is_file($file)) {
        return false;
    }
}

require __DIR__ . '/../vendor/autoload.php';

session_start();

// Instantiate the app
$settings = require __DIR__ . '/../src/settings.php';
$app = new \Slim\App($settings);

// Set up dependencies
require __DIR__ . '/../src/dependencies.php';

// Register middleware
require __DIR__ . '/../src/middleware.php';

// Register routes
// require __DIR__ . '/../src/routes.php';

/////// DESDE AQUI EMPEZAMOS A REALIZAR NUESTRO PROYECTO
/////// Establecemos parametros iniciales
// $app->contentType('text/html; charset=utf-8');

//Creamos una conexion a bbdd
define('BD_SERVIDOR', 'localhost');
define('BD_NOMBRE', 'agenda');
define('BD_USUARIO', 'root');
define('BD_PASSWORD', '');

$db = new PDO('mysql:host=' . BD_SERVIDOR . ';dbname=' . BD_NOMBRE . ';charset=utf8', BD_USUARIO, BD_PASSWORD);

//A partir de aqui, podemos ir creando las diferentes rutas get, post, etc etc a nuestro web service:

$app->get('/', function(){
	?>
	<h1>Bienvenidos a mi webservice</h1>
	<table>
		<tr>
			<th>URL</th>
			<th>VERBO</th>
			<th>Devolucion</th>
			<th>Envio</th>
		</tr>
		<tr>
			<td>http://192.168.1.58/slim/public/</td>
			<td>SIN VERBO</td>
			<td>Instrucciones de la aplicacion</td>
			<td>NADA</td>
		</tr>
		<tr>
			<td>http://192.168.1.58/slim/public/contactos</td>
			<td>GET</td>
			<td>DATOS EN FORMATO JSON</td>
			<td>NADA</td>
		</tr>
		<tr>
			<td>http://192.168.1.58/slim/public/contactos/ID</td>
			<td>GET</td>
			<td>DATOS EN FORMATO JSON</td>
			<td>Recibe por la URL el ID del contacto que quiero mostrar</td>
		</tr>
		<tr>
			<td>http://192.168.1.58/slim/public/contactos/buscar/CAMPO/VALOR</td>
			<td>GET</td>
			<td>DATOS EN FORMATO JSON</td>
			<td>Recibe por la URL el CAMPO por el que filtrar, y el VALOR por el que se filtrara</td>
		</tr>
		<tr>
			<td>http://192.168.1.58/slim/public/contactos</td>
			<td>POST</td>
			<td>DATOS EN FORMATO JSON</td>
			<td>Tenemos que enviarle los datos por post, que queremos insertar en la bbdd. nombre, apellidos, email, telefono</td>
		</tr>
		<tr>
			<td>http://192.168.1.58/slim/public/contactos2</td>
			<td>POST</td>
			<td>DATOS EN FORMATO JSON</td>
			<td>Tenemos que enviarle los datos por post, que queremos insertar en la bbdd. nombre, apellidos, email, telefono</td>
		</tr>
		
		<tr>
			<td>http://192.168.1.58/slim/public/contactos/ID</td>
			<td>DELETE</td>
			<td>DATOS EN FORMATO JSON</td>
			<td>Recibe por la URL el ID del contacto que quiero borrar</td>
		</tr>

	</table>
	<?php

});

$app->get('/contactos', function() use($db){

	$sql="SELECT * FROM contactos";
	$consulta = $db->prepare($sql);
	$consulta->execute();
	// Almacenamos los resultados en un array asociativo.
	$resultados = $consulta->fetchAll(PDO::FETCH_ASSOC);
	$respuesta=json_encode($resultados);
	echo $respuesta;

});

//$app->get('/contactos/{id}', function(Request $request, Response $response, $args) use($db){

$app->get('/contactos/{id}', function ($request, $response) use($db){

	$sql="SELECT * FROM contactos WHERE id=:id";
	$consulta=$db->prepare($sql);
	$id=$request->getAttribute('id');
	$consulta->execute(array(':id' => $id));
	$resultados = $consulta->fetchAll(PDO::FETCH_ASSOC);
	$respuesta=json_encode($resultados);
	echo $respuesta;

});

$app->get('/contactos/buscar/{campo}/{valor}', function ($request, $response) use($db){
	//nombre, apellidos, email, telefono
	$campo=$request->getAttribute('campo');
	$valor=$request->getAttribute('valor');
	//$sql="SELECT * FROM contactos WHERE :campo=:valor";
	$sql="SELECT * FROM contactos WHERE $campo='$valor'";
	$consulta=$db->prepare($sql);
	//$consulta->execute(array(':campo'=> $campo,':valor'=> $valor));
	$consulta->execute();
	$resultados = $consulta->fetchAll(PDO::FETCH_ASSOC);
	$respuesta=json_encode($resultados);
	echo $respuesta;
});

$app->post('/contactos', function ($request, $response) use($db){
	//Traemos los datos que queremos enviar
	$datos=$request->getParsedBody();
	$nombre=$datos['nombre'];
	$apellidos=$datos['apellidos'];
	$email=$datos['email'];
	$telefono=$datos['telefono'];
	$sql="INSERT INTO contactos(nombre, apellidos, email, telefono)VALUES('$nombre', '$apellidos', '$email', '$telefono')";
	$consulta=$db->prepare($sql);

	$estado=$consulta->execute();

	if ($estado){
		echo json_encode(array('estado' => true, 'mensaje' => 'Datos insertados correctamente.'));
	}else{
		echo json_encode(array('estado' => false, 'mensaje' => 'Error al insertar datos en la tabla.'));
	}
});

$app->post('/contactos2', function ($request, $response) use($db){
	//Traemos los datos que queremos enviar
	$datos=$request->getParsedBody();

	$nombre=$datos['nombre'];
	$apellidos=$datos['apellidos'];
	$email=$datos['email'];
	$telefono=$datos['telefono'];
	
	$consulta = $db->prepare("insert into contactos(nombre,apellidos,email,telefono) 
					values (:nombre,:apellidos,:email,:telefono)");
	$estado = $consulta->execute(
		   array(
			  ':nombre' => $nombre,
			  ':apellidos' => $apellidos,
			  ':email' => $email,
			  ':telefono' => $telefono
		   )
	);
	if ($estado){
		echo json_encode(array('estado' => true, 'mensaje' => 'Datos insertados correctamente.'));
	}else{
		echo json_encode(array('estado' => false, 'mensaje' => 'Error al insertar datos en la tabla.'));
	}
});

$app->delete('/contactos/{id}', function ($request, $response) use($db){

	$id=$request->getAttribute('id');
	$consulta = $db->prepare("DELETE FROM contactos where id=:id");
	$consulta->execute(array(':id' => $id));

	if ($consulta->rowCount() == 1){
		echo json_encode(array('estado' => true, 'mensaje' => 'El usuario ' . $id . ' ha sido borrado correctamente.'));
	}else{
		echo json_encode(array('estado' => false, 'mensaje' => 'ERROR: ese registro no se ha encontrado en la tabla.'));
	}
});


// Actualización de datos de usuario (PUT)
$app->put('/contactos/{id}', function ($request, $response) use($db){
	// Para acceder a los datos recibidos del formulario
	$id=$request->getAttribute('id');

	$datos=$request->getParsedBody();
	$nombre=$datos['nombre'];
	$apellidos=$datos['apellidos'];
	$email=$datos['email'];
	$telefono=$datos['telefono'];

	$consulta = $db->prepare("update contactos set nombre=:nombre, apellidos=:apellidos, email=:email, telefono=:telefono where id=:id");

	$estado = $consulta->execute(
		   array(
			':id' => $id,
			':nombre' => $nombre,
			':apellidos' => $apellidos,
			':email' => $email,
			':telefono' => $telefono
		   )
	);

	// Si se han modificado datos...
	if ($consulta->rowCount() == 1){
		echo json_encode(array('estado' => true, 'mensaje' => 'Datos actualizados correctamente.'));
	}else{
		echo json_encode(array('estado' => false, 'mensaje' => 'Error al actualizar datos, datos 
						no modificados o registro no encontrado.'));
	}
});

// Run app
$app->run();
